FROM node

RUN useradd -m user \
  && apt-get -qy update \
  && apt-get -qy install nginx \
    awscli chromium

USER    user
WORKDIR /home/user

ENV  NODE_ENV production
COPY npm-shrinkwrap.json package.json ./
RUN  npm install
ENV  CHROME_PATH /usr/bin/chromium
