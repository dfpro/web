# S3 + CloudFront setup

## Create Stack
### stage/dev
```
aws --region us-east-1 cloudformation create-stack \
  --stack-name devLatencyAt \
  --template-body "$(cat site.yml)"
```

### prod
```
aws --region us-east-1 cloudformation create-stack \
  --stack-name LatencyAt \
  --parameters ParameterKey=Domain,ParameterValue=latency.at \
  --template-body "$(cat site.yml)"
```

## Update Stack
### stage/dev
```
aws --region us-east-1 cloudformation update-stack \
  --stack-name devLatencyAt \
  --template-body "$(cat site.yml)"
```

## prod
```
aws --region us-east-1 cloudformation update-stack \
  --stack-name LatencyAt \
  --parameters ParameterKey=Domain,ParameterValue=latency.at \
  --template-body "$(cat site.yml)"
```
