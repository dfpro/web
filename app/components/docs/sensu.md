Sensu can ingest Metrics from Latency.at by using the [Sensu Prometheus
Collector](https://github.com/portertech/sensu-prometheus-collector#sensu-prometheus-collector).

The Latency.at metrics can be consumed as any other Prometheus endpoint, just
make sure to specify the Authorization header in the check definition:

```
  "checks": {
    "prometheus_metrics": {
      "type": "metric",
      "command": "sensu-prometheus-collector -export-authorization 'Bearer your-token' \
        -exporter-url https://nyc1.do.mon.latency.at/probe?module=http_2xx&target=https%3A%2F%2Fapi.latency.at%2F",
      "subscribers": ["test_tier"],
      "interval": 10,
      "handler": "influx"
    }
```
