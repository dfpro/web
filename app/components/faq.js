import React from 'react';
import {Link} from 'react-router';
import Panel from './panel';

const FAQ = () => {
  return <div id="faq">
    <article>
      <div className="wrapper-wide">
        <h1>Frequently asked Questions</h1>
      </div>
    </article>
    <article>
      <h2>Service</h2>
      <Panel title="How does it work?">
        <p>Latency.at runs a set of probes around the world. Your Prometheus
        server can request performance and availability metrics about arbitrary
        targets from these probes. The requests are authenticated by a token
        which you can generate after <Link to="/signup">signing up</Link>.
        </p>
        <h5>Example</h5>
        <p>
          Let's say you want to get HTTPS metrics about the site&nbsp;
          <em>example.com</em> as seen from a probe running in Singapore at
          DigitalOcean. For that you would configure
          Prometheus to scrape:
          <br/>
          <a href="https://sgp1.do.mon.latency.at/probe?module=http_2xx&target=https%3A%2F%2Fexample.com">
          https://sgp1.do.mon.latency.at/probe?module=http_2xx&target=https%3A%2F%2Fexample.com
          </a>
          <br/>
          At your defined scrape interval, Prometheus will connect to this URL.
          The prober handles the request by connecting to the specified target,
          using the specified module and return metrics about it. These metrics
          are now available in your server to be aggregated, correlated and
          graphed.</p>
        <p>
          For this to work you need to set `bearer_token` to the auth token you
          generated in the dashboard. For more details, see the <Link
          to="/configuration">configuration section</Link>.
        </p>
      </Panel>
      <Panel title="What metrics are provided?">
        <p>Depending on the <Link to="/configuration">target
        configuration</Link>, we return different metrics for HTTP, HTTPS, TCP
        and ICMP connections:
        </p>
        <table>
          <thead><tr><td>Type</td><td>Name</td><td>Description</td></tr></thead>
          <tbody>
            <tr><td>*</td><td>probe_success</td><td>
              1 if request was successful</td></tr>
            <tr><td>*</td><td>probe_duration</td><td>
              Duration of the request in seconds</td></tr>
            <tr><td>*</td><td>probe_ip_protocol</td><td>
              IP protocol version used (4 or 6)</td></tr>
            <tr><td>http</td><td>probe_http_duration_seconds</td><td>
              Duration of http request by phase, summed across all redirects
              (if any)</td></tr>
            <tr><td>http</td><td>probe_http_status_code</td><td>
              Status Code of response</td></tr>
            <tr><td>http</td><td>probe_http_redirects</td><td>
              Number of redirects followed during request</td></tr>
            <tr><td>http</td><td>probe_content_length</td><td>
              Content length in response header</td></tr>
            <tr><td>http(s)</td><td>probe_http_ssl</td><td>
              1 if HTTPS is used</td></tr>
            <tr><td>https</td><td>probe_ssl_earliest_cert_expiry</td><td>
              Timestamp of earliest expiring certificate</td></tr>
          </tbody>
        </table>
        <p>We will continuously extend the metrics we provide. If you're
          interested in specific protocols or metrics,&nbsp;
          <Link to="/contact">let us know.</Link></p>
      </Panel>

      <Panel title="What probe locations are available?">
        <p>We operate probes in San Francisco, New York,
        Frankfurt, Bangalore and Singapore at DigitalOcean. For
        details see the <Link to="/configuration">configuration section
        </Link>.
        Soon we'll introduce more regions across multiple cloud
        providers. We plan to provide coverage in all regions served by
        the major cloud providers.</p>
      </Panel>

      <Panel title="How do I get the duration of all successful requests?">
      <p>The `probe_duration` metric reflects the duration of the request, no
        matter whether it was successful or not. To calculate the duration of
        successful, or failed, requests only, use <a
        href="https://prometheus.io/docs/querying/operators/#binary-operators">binary
        operators.</a></p>
      <p>Calculate duration of successful requests:
        <pre>probe_duration_seconds * (probe_success == 1)</pre>
      </p>
      <p>Calculate duration of failed requests:
        <pre>probe_duration_seconds + (probe_success == 0)</pre>
      </p>
      </Panel>


      <Panel title="How does this service relate to the Prometheus Blackbox
        Exporter?">
      <p>Our Exporter is based on the official
        <a href="https://github.com/prometheus/blackbox_exporter"> Prometheus
        Blackbox Exporter</a>. Beside operational metrics and integration in
        our accounting, we we provide more detailed HTTP metrics, most notable
        breakdown by request phases and redirect. We will try to contribute
        these changes back to the official Blackbox Exporter or release our
        open source exporter soon.</p>
      </Panel>

      <div style={{marginTop: '2em'}} />
      <h2>Billing</h2>
      <Panel title="How do you bill me?">
      <p>We're using Stripe to securly process your payments. After adding a
      credit card, you can subscribe to on of our plans.</p>

      <p>On each renewal your request balance gets topped up by the given
      number of requests. Requests from the prior subscription cycle expire.</p>
      </Panel>

      <Panel title="How do I get exempt from VAT as business?">
      <p>We are required to collect VAT from individual customer within the EU.
      If you're a business you can enter your <a
      href="https://en.wikipedia.org/wiki/VAT_identification_number">VAT
      identification number</a> in the dashboard's billing section to get
      exempt.</p>

      <p>For existing subscription, the exemption get applies on the next
      billing cycle.</p>
      </Panel>

      <Panel title="What happens if I upgrade?">
      <p>You can subscribe to a bigger plan any time and we will prorate the
      subscription costs and start a new billing cycle. This will <b>add</b> the
      number of request of the new plan to your account balance.</p>

      <p><b>Example:</b> You're on the $50 plan and decide to upgrade to the
      $100 plan in the middle of your billing cycle. We will credit you $50 for
      the unused portion of the billing cycle, charge you the different of $50
      and start a new billing cycle. This will <b>set</b> your balance to
      15,000,000. One month later, the subscription renews and you get charged
      the full $100.</p>
      </Panel>

      <Panel title="What happens if I downgrade?">
      <p>You can also downgrade at any time, taking effect in the next billing
      cycle.</p>

      <p>After downgrading you can still use up all your remaining credits
      until the end of your billing cycle. On the next cycle, you get charged
      the price of your new plan and unused credits expire.</p>
      </Panel>


      <Panel title="What happens if I cancel my subscription?">
      <p>If you cancel your subscription, you can still use up all your
      remaining credits until the end of your billing cycle. After that, they'll
      expire and you won't be able to use the service anymore.</p>
      </Panel>

    </article>
  </div>;
};

export default FAQ;
FAQ.propTypes = {
};
