import React from 'react';

import ReactGA from 'react-ga';

const AlfImage = require('../images/alf.gif');

const NoMatch = () => {
  ReactGA.event({
    category: 'page',
    action: 'not_found',
  });

  return <div className="wrapper">
    <h1>Not Found</h1>
    <h2>Sorry, this URL doesn't exist</h2>
    <p>
      <img style={{width: '100%'}} src={AlfImage}
        alt="AlfTheCat, employee of the month"/>
      No worries, we monitor dead links and will fix this soon. We're a
      monitoring company after all, right?
    </p>
  </div>;
};
export default NoMatch;
