import React from 'react';
import PropTypes from 'prop-types';
import {Link, browserHistory} from 'react-router';
import defaultPlans from './data/plans';
import Plan from './plan';

export default class Plans extends React.Component {
  constructor() {
    super();
    this.getPlans = this.getPlans.bind(this);
    this.subscribe = this.subscribe.bind(this);
    this.unsubscribe = this.unsubscribe.bind(this);
    this.state = {plans: defaultPlans};
  }
  componentWillMount() {
    this.getPlans();
  }
  getPlans() {
    this.props.api.Plans.get()
    .then((plans) => {
      this.setState({
        plans: Object.values(plans),
      });
    }).catch(this.props.loadFailed);
  }
  subscribe(plan) {
    if (!this.props.card) { // card needed
      this.props.notify('Credit card required', 'success');
      browserHistory.push('/dashboard/billing');
      return;
    }

    this.props.api.Plans.subscribe(plan.id)
    .then((resp) => {
      this.props.notify('Successfully subscribed to ' + plan.id + '.',
        'success');
			this.props.update && this.props.update();
    }).catch(this.props.loadFailed);
  }
  unsubscribe() {
    this.props.api.User.unsubscribe()
    .then((resp) => {
      this.props.notify('Successfully unsubscribed', 'success');
			this.props.update && this.props.update();
    }).catch(this.props.loadFailed);
  }
  render() {
    let subText = this.props.subscription ? 'Downgrade' : 'Subscribe';
    let plans = this.state.plans.sort((a, b) => {
      return a.amount - b.amount;
    }).map((plan, i) => {
      let className = '';
      if (!this.props.subscription && i == 0) {
        className = '';
      }
      const handler = this.props.onClick || this.subscribe;
      // disabled={this.props.card == null}
      let sc = <button className={className}
        onClick={() => {
          handler(plan);
      }}>
        {subText}
      </button>;

      className = 'plan-box';
      if (this.props.subscription &&
          plan.id == this.props.subscription.plan.id
      ) {
        subText = 'Upgrade';
        className = 'plan-box active';
        let disabled = (plan.id == 'free');
        sc = <button onClick={this.unsubscribe} disabled={disabled}>Cancel
          </button>;
      }
      return <Plan plan={plan}>{sc}</Plan>;
    });
    if (!this.props.subscription) {
      subText = 'Active';
    }

    if (this.props.hideInfo) {
      return <div id="plans" className="clearfix">
        {plans}
      </div>;
    }

    let vatNote;
    if (this.props.inEU && !this.props.user.vatin) {
      vatNote = <p>Price plus {this.props.vatRate*100}% VAT.&nbsp;
        <Link to="/dashboard/billing">Enter VAT ID Number</Link> to remove VAT
        from bill.</p>;
    }

    return <div className="wrapper-wide">
      <div id="plans" className="clearfix">
        {plans}
      </div>
      <div className="center">
        {vatNote}
        <p className="fixed-footer">
          Need a custom plan?&nbsp;
          <a href="mailto:sales@latency.at">Talk to use</a>.
        </p>
      </div>
    </div>;
  }
};

Plans.propTypes = {
  api: PropTypes.object,

  notify: PropTypes.func,
  update: PropTypes.func,

  onClick: PropTypes.func,
  loadFailed: PropTypes.func,

  user: PropTypes.object,
  card: PropTypes.object,
  inEU: PropTypes.bool,
  vatRate: PropTypes.number,
  subscription: PropTypes.object,
  hideInfo: PropTypes.bool,
};
