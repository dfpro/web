import React from 'react';
import PropTypes from 'prop-types';
import {browserHistory} from 'react-router';

export default class Reset extends React.Component {
  constructor() {
    super();
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.getPasswordValidationState =
      this.getPasswordValidationState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      password: '',
    };
  }
  handleChangePassword(e) {
    this.setState({password: e.target.value});
  }
  getPasswordValidationState() {
    if (this.state.password.length > 7) {
      return 'success';
    } else {
      return 'error';
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.api.User.reset(this.props.params.token, this.state.password)
    .then((data) => {
      this.props.notify('Password successfully set!', 'success');
      browserHistory.push('/login');
    })
    .catch((error) => {
      this.props.notify(error.message, 'danger');
    });
  }
  classFor(obj) {
    return 'control-label ' + (obj.length > 0 ? 'control-label-up' : '');
  }
  render() {
    return (
      <div className="dashboard light">
        <div>
          <h1>Reset your password</h1>
          <div className="wrapper">
            <form onSubmit={this.handleSubmit}>
               <div className="form-group">
                <label htmlFor="password"
                  className={this.classFor(this.state.password)}>
                  New Password
                </label>
                <input
                    style={{marginBottom: '1em'}}
                    type="password"
                    id="password"
                    placeholder="New Password"
                    onChange={this.handleChangePassword}/>
                <button type="submit" className="button-primary">Set password
                  </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
};

Reset.propTypes = {
  api: PropTypes.object,
  modalShow: PropTypes.func,
  params: PropTypes.object.isRequired,
  notify: PropTypes.func,
};
