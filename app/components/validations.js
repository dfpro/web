// eslint-disable-next-line max-len
let emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

export function validEmail(address) {
  return emailRegex.test(address);
};

export function validateEmail(address) {
  return validEmail(address) ? 'success' : 'error';
};

export function validPassword(password) {
  return password.length > 7;
};

export function validatePassword(password) {
  return validPassword(password) ? 'success' : 'error';
};

