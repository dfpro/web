import React from 'react';
import PropTypes from 'prop-types';
import Alert from './alert';

import config from 'config';

import ReactPixel from 'react-facebook-pixel';
ReactPixel.init(config.pixelID);

import {browserHistory} from 'react-router';

export default class Activate extends React.Component {
  constructor() {
    super();
    this.state = {
      style: null,
      message: null,
    };
  }
  componentWillMount() {
    this.props.api.User.activate(this.props.params.token)
    .then((json) => {
      this.props.api.setAuthToken(json.token);
      this.props.notify('Account successfully activate!', 'success');
      this.props.update();
      ReactPixel.track('CompleteRegistration');
      browserHistory.push('/dashboard');
    })
    .catch((error) => {
      this.props.notify(error.message, 'danger');
    });
  }
  render() {
    return (
      <div>
        <h1>Activate your Account</h1>
        <Alert bsStyle={this.state.style}>{this.state.message}</Alert>
      </div>
    );
  }
};

Activate.propTypes = {
  notify: PropTypes.func,
  update: PropTypes.func,
  api: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
};
