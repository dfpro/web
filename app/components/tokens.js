import React from 'react';
import PropTypes from 'prop-types';
import ClipboardButton from 'react-clipboard.js';

export default class Tokens extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tokens: [],
    };
    this.addToken = this.addToken.bind(this);
  }
  componentWillMount() {
    this.getTokens();
  }
  getTokens() {
    this.props.api.Tokens.get()
    .then((tokens) => {
      this.setState({tokens: tokens.tokens});
    })
    .catch((error) => {
      this.props.loadFailed(error);
    });
  }
  deleteToken(token) {
    this.props.api.Tokens.delete(token.id)
    .then(() => {
      this.getTokens();
    })
    .catch((error) => {
      this.props.loadFailed(error);
    });
  }
  addToken(e) {
    e.preventDefault();
    this.props.api.Tokens.create()
    .then(() => {
      this.getTokens();
    })
    .catch((error) => {
      this.props.loadFailed(error);
    });
  }
  render() {
    let r = this;
    let tokens = this.state.tokens.map((token) => {
      let cd = new Date(token.created);

      // FIXME: If the row gets moved under the cursor when deleting, the
      // mouseEnter event doesn't fire.
      return (
        <tr key={token.id}
          onMouseEnter={() => {
            this.setState({hover: token.id});
          }}
          onMouseLeave={() => {
            this.setState({hover: null});
          }}
        >
          <td id={token.id} className="token">
            <span>{token.token}</span>
            <ClipboardButton component='a'
              data-clipboard-text={token.token}>
              Copy
            </ClipboardButton>
          </td>
          <td>
            <span className="timestamp">{cd.toLocaleDateString()}</span>
            <a href='#'
               onClick={r.deleteToken.bind(r, token)}>
              Delete
            </a>
          </td>
        </tr>
      );
    });

    if (tokens.length == 0) {
      if (this.props.user.activated) {
        return <div id="tokens" className="wrapper">
            <div className="center cta"><p>Add a Token for your Prometheus
              Configuration to get started.</p>
              <button className="button-primary" onClick={r.addToken}>Add Token
              </button>
            </div>
          </div>;
      }
      return <div id="tokens">
        <div className="center cta">
          <p>Please check your email to activate your account and add Tokens.
          </p>
          <button className="button-primary"
            onClick={this.props.sendActivation}>Resent Confirmation</button>
        </div>
      </div>;
    }

    return <div id="tokens">
      <table>
        <thead>
          <tr>
            <th>Token</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {tokens}
        </tbody>
      </table>
      <button className="fixed-br button-primary" onClick={r.addToken}>
        Add Token
      </button>
    </div>;
  }
};

Tokens.propTypes = {
  // Required but leading to false positives due to usage of cloneElement.
  api: PropTypes.object,
  notify: PropTypes.func,
  loadFailed: PropTypes.func,
  sendActivation: PropTypes.func,
  user: PropTypes.object,
};
