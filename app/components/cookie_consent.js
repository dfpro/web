import React from 'react';
import {Link} from 'react-router';

const style = {
  position: 'absolute',
  zIndex: '1000',
  width: '100%',
  fontSize: '75%',
  backgroundColor: 'black',
  color: 'white',
  padding: '1em 0em',
  textAlign: 'center',
};

export default class CookieConsent extends React.Component {
  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.state = {
      confirmed: localStorage.getItem('cookies-confirmed') == 'true',
    };
  }
  close() {
    localStorage.setItem('cookies-confirmed', 'true');
    this.setState({confirmed: true});
  }
  render() {
    if (this.state.confirmed) {
      return <div></div>;
    }
    return <div style={style}><span>
      This website uses cookies to ensure you get the best
      experience on our website. <Link to="/legal/privacy#cookies">Learn
      more.</Link>
      <a style={{padding: '0 2em', fontWeight: 'bold'}}
        href="#" onClick={this.close}>&times;</a>
      </span>
    </div>;
  }
};
