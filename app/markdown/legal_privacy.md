#### Privacy Policy
It is latency.at's policy to respect your privacy regarding any information we
may collect while operating our website. Accordingly, we have developed this
privacy policy in order for you to understand how we collect, use, communicate,
disclose and otherwise make use of personal information. We have outlined our
privacy policy below.

1.  We will collect personal information by lawful and fair means and, where
    appropriate, with the knowledge or consent of the individual concerned.
2.  Before or at the time of collecting personal information, we will identify
    the purposes for which information is being collected.
3.  We will collect and use personal information solely for fulfilling those
    purposes specified by us and for other ancillary purposes, unless we obtain
    the consent of the individual concerned or as required by law.
4.  Personal data should be relevant to the purposes for which it is to be used,
    and, to the extent necessary for those purposes, should be accurate,
    complete, and up-to-date.
5.  We will protect personal information by using reasonable security safeguards
    against loss or theft, as well as unauthorized access, disclosure, copying,
    use or modification.
6.  We will make readily available to customers information about our policies
    and practices relating to the management of personal information.
7.  We will only retain personal information for as long as necessary for the
    fulfilment of those purposes.

We are committed to conducting our business in accordance with these principles
in order to ensure that the confidentiality of personal information is protected
and maintained. latency.at may change this privacy policy from time to time at
latency.at's sole discretion.

This Privacy Policy is based on [getterms.io](http://getterms.io/)

### Cookies
To make this site work properly, we sometimes place small data files called
cookies on your device. Most big websites do this too.

#### What are cookies?

A cookie is a small text file that a website saves on your computer or mobile
device when you visit the site. It enables the website to remember your actions
and preferences (such as login, language, font size and other display
preferences) over a period of time, so you don’t have to keep re-entering them
whenever you come back to the site or browse from one page to another.

#### How do we use cookies?
We use cookies to remember your login and preferences.  We also use cookies to
learn about our users by using services like Google Analytics.

#### How to control cookies
You can control and/or delete cookies as you wish – for details, see
[aboutcookies.org](http://aboutcookies.org). You can delete all cookies that are
already on your computer and you can set most browsers to prevent them from
being placed. If you do this, however, you may have to manually adjust some
preferences every time you visit a site and some services and functionalities
may not work.
